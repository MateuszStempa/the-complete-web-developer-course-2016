<?php

$i = 5;

while ($i <= 50) {
    
    echo $i."<br>";
    
    $i = $i + 5;
    
}

echo "<br><br>";

$family = array("Mateusz", "Rafał", "Tomek", "Bartek", "Kamil");

$i = 0;

while ($i < sizeof($family)) {
    
    echo $family[$i]."<br>";
    
    $i++;
    
}