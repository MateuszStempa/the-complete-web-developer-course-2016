<?php
$weather = "";
$error = "";

if (array_key_exists('city', $_GET)) { 
    
    $city = str_replace(' ', '', str_replace("'",'', iconv('utf-8','us-ascii//TRANSLIT//IGNORE', $_GET['city'])));

    $file_header = @get_headers("https://www.meteoprog.pl/pl/weather/" . $city . "/");
    
//    print_r($file_header);
    
    
    if (array_key_exists('HTTP/1.1 404 Not Found', $file_header)) {

        $error = "Nie można znaleźć miasta.";
    }
    else if ($file_header[0] == 'HTTP/1.1 404 Not Found') {

        $error = "Nie można znaleźć miasta.";
    }
    else if ($file_header[15] == 'HTTP/1.1 404 Not Found') {

        $error = "Nie można znaleźć miasta.";
    }
    
    else {

        $forecastPage = file_get_contents("https://www.meteoprog.pl/pl/weather/" . $city . "/");

        $pageArray = explode('title="Pogoda', $forecastPage);

        if (sizeof($pageArray) > 1) {

            $secondPageArray = explode('" alt="Pogoda ', $pageArray[1]);

            if (sizeof($secondPageArray) > 1) {

                $weather = $secondPageArray[0];
            }
            else {

                $error = "Nie można znaleźć miasta.";
            }
        }
        else {

            $error = "Nie można znaleźć miasta.";
        }
    }
    
    
    
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Pogodynka</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <style type="text/css">
            html { 
                background: url(background.jpg) no-repeat center center fixed; 
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }
            body {
                background: none;
            }
            .container {
                text-align: center;
                margin-top: 200px;
                width: 450px;
            }
            input {
                margin: 20px 0;
            }
            #weather {
                margin-top: 15px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Jaka jest pogoda?</h1>
            <form>
                <div class="form-group">
                    <label for="city">Wpisz nazwę miasta</label>
                    <input type="text" class="form-control" id="city" name="city" placeholder="np: Gdańsk" value="<?php if (array_key_exists('city', $_GET)) { echo $_GET['city']; } ?>">
                </div>
                <button type="submit" class="btn btn-primary">Sprawdź</button>
            </form>
            <div id="weather">
                <?php
                if ($weather) {
                    echo '<div class="alert alert-info" role="alert"><strong>' . $weather . '</strong></div>';
                }
                else if ($error) {

                    echo '<div class="alert alert-danger" role="alert"><strong>' . $error . '</strong></div>';
                }
                ?>
            </div>
        </div>

        <!-- jQuery first, then Tether, then Bootstrap JS. -->
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    </body>
</html>