<?php

for ($i = 10; $i >= 0; $i--) {
    
     echo $i."<br>";
    
}

echo "<br><br>";

$family = array("Mateusz", "Rafał", "Tomek", "Bartek", "Kamil");

for ($i = 0; $i < sizeof($family); $i++) {
    
    echo $family[$i]."<br>";
    
}

echo "<br><br>";

foreach ($family as $key => $value) {
    
    $family[$key] = $value." Nowak";
    
    echo "Elementem tablicy na miejscu ".$key." jest ".$value."<br>";
    
}

echo "<br><br>";

for ($i = 0; $i < sizeof($family); $i++) {
    
    echo $family[$i]."<br>";
    
}

