<?php

$error = "";
$successMessage = "";

if ($_POST) {

    if (!$_POST["email"]) {

        $error .= "Wymagany jest adres email.<br>";
    }

    if (!$_POST["content"]) {

        $error .= "Wymagana jest treść wiadomości.<br>";
    }

    if (!$_POST["subject"]) {

        $error .= "wymagany jest temat wiadomości.<br>";
    }

    if ($_POST['email'] && filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) === false) {

        $error .= "Adres email jest nieprawidłowy!.<br>";
    }

    if ($error != "") {

        $error = '<div class="alert alert-danger" role="alert"><p><strong>Uzupełnij pola:</strong></p>' . $error . '</div>';
    }
    else {

        $emailTo = "matii.php.dev@gmail.com";

        $subject = $_POST['subject'];

        $headers = "od: " . $_POST['email'];

        $content = "Wiadomość $headers brzmi: " . $_POST['content'];

        if (mail($emailTo, $subject, $content, $headers)) {

            $successMessage = '<div class="alert alert-success" role="alert">Wiadomość została wysłana.</div>';
        }
        else {

            $error = '<div class="alert alert-danger" role="alert"><p><strong>Twoja wiadomość nie została wysłana, proszę spróbować później.</div>';
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags always come first -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
    </head>
    <body>

        <div class="container">

            <h1>Get in touch!</h1>

            <div id="error"><?php echo $error . $successMessage; ?></div>

            <form method="post">
                <fieldset class="form-group">
                    <label for="email">Adres email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Wpisz twój adres email">
                    <small class="text-muted">Nigdy nie udostępniamu twojego adresu email innym podmiotom.</small>
                </fieldset>
                <fieldset class="form-group">
                    <label for="subject">Temat</label>
                    <input type="text" class="form-control" id="subject" name="subject" >
                </fieldset>
                <fieldset class="form-group">
                    <label for="exampleTextarea">O co chciałbyś zapytać?</label>
                    <textarea class="form-control" id="content" name="content" rows="3"></textarea>
                </fieldset>
                <button type="submit" id="submit" class="btn btn-primary">Wyślij</button>
            </form>
        </div>

        <!-- jQuery first, then Bootstrap JS. -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>

        <script type="text/javascript">

            $("form").submit(function (e) {

                var error = "";

                if ($("#email").val() == "") {

                    error += "Wpisz adres email.<br>";
                }

                if ($("#subject").val() == "") {

                    error += "Wpisz temat wiadomości.<br>";
                }

                if ($("#content").val() == "") {

                    error += "Wpisz treść wiadomości.<br>";
                }

                if (error != "") {

                    $("#error").html('<div class="alert alert-danger" role="alert"><p><strong>Uzupełnij pola:</strong></p>' + error + '</div>');

                    return false;

                } else {

                    return true;

                }
            });

        </script>
    </body>
</html>