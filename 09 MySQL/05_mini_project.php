<?php
$error = "";
$successMessage = "";

if ($_POST) {

    if (!$_POST["email"]) {

        $error .= "Wymagany jest adres email.<br>";
    }

    if (!$_POST["password"]) {

        $error .= "Wymagany jest hasło.<br>";
    }

    if ($_POST['email'] && filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) === false) {

        $error .= "Adres email jest nieprawidłowy!.<br>";
    }

    if ($error != "") {

        $error = '<div class="alert alert-danger" role="alert"><p><strong>Uzupełnij pola:</strong></p>' . $error . '</div>';
    }
    else {

        $host = "localhost";
        $dbUser = "hit";
        $dbPassword = "hit";
        $dbName = "users";

        $link = mysqli_connect($host, $dbUser, $dbPassword, $dbName);

        if (mysqli_connect_error()) {

            die("Wystąpił błąd podczas połączenia z bazą danych.");
        }
        else {

            $email = $_POST["email"];
            $password = $_POST["password"];

            $query = "SELECT * FROM users WHERE email = '" . mysqli_real_escape_string($link, $email) . "'";
        }

        $result = mysqli_query($link, $query);

        if (mysqli_num_rows($result) > 0) {

            $error = '<div class="alert alert-danger" role="alert"><p><strong>Taki email już istnieje w bazie!</div>';
        }
        else {

            $query = "INSERT INTO `users` (`email`, `password`) VALUES ('" . 
                    mysqli_real_escape_string($link, $email) . "','" . 
                    mysqli_real_escape_string($link, $password) . "') ";

            if (mysqli_query($link, $query)) {

                $successMessage = '<div class="alert alert-success" role="alert">Zarejestrowano pomyślnie.</div>';
            }
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags always come first -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">
    </head>
    <body>

        <div class="container">


            <div id="error"> <?php echo $error . $successMessage; ?> </div>

            <h1>Zarejestruj się.</h1>
            <form method="post">
                <fieldset class="form-group">
                    <label for="email">Adres email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Wpisz adres email">
                    <small class="text-muted">Nigdy nie udostępniamu twojego adresu email innym podmiotom.</small>
                </fieldset>
                <fieldset class="form-group">
                    <label for="subject">Hasło</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Wpisz hasło" >
                </fieldset>
                <button type="submit" id="submit" class="btn btn-primary">Zarejestruj</button>
            </form>
        </div>

        <!-- jQuery first, then Bootstrap JS. -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>

        <script type="text/javascript">

            $("form").submit(function (e) {

                var error = "";

                if ($("#email").val() == "") {

                    error += "Wpisz adres email.<br>";
                }

                if ($("#password").val() == "") {

                    error += "Wpisz hasło.<br>";
                }

                if (error != "") {

                    $("#error").html('<div class="alert alert-danger" role="alert"><p><strong>Uzupełnij pola:</strong></p>' + error + '</div>');

                    return false;

                } else {

                    return true;

                }
            });

        </script>
    </body>
</html>