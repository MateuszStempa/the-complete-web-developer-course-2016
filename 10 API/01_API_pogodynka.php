<?php
$weather = "";
$error = "";

if (array_key_exists('city', $_GET)) {

    
    $urlContents = @file_get_contents("http://api.openweathermap.org/data/2.5/weather?q=" . urlencode($_GET['city'])
            . "&appid=90e5290289dad6e2b71512d613173f75");
        
    $weatherArray = json_decode($urlContents, true);

    if ($weatherArray['cod'] == 200) {

//        $weather = "Pogoda w " . $_GET['city'] . " jest '" . $weatherArray['weather'][0]['description'] . "'. ";
        
        $weather = "<strong>Pogoda w ".$weatherArray['name'].":</strong><br>";

        $tempInCelcius = intval($weatherArray['main']['temp'] - 273);

        $windSpeed = intval($weatherArray['wind']['speed']);
        
        $clouds = intval($weatherArray['clouds']['all']);
        
        $pressure = intval($weatherArray['main']['pressure']);
        
        $weather .= " Temperatura wynosi " . $tempInCelcius . "&deg;C<br>";

        $weather .= "Prędkośc wiatru wynosi " . $windSpeed . "m/s<br>";
        
        $weather .= "Ciśnienie " . $clouds . "hPa<br>";
        
        $weather .= "Zachmurzenie " . $clouds . "%<br>";
    }
    else {

        $error = "Nie znaleziono miasta.";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Pogodynka</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <style type="text/css">
            html { 
                background: url(background.jpg) no-repeat center center fixed; 
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }
            body {
                background: none;
            }
            .container {
                text-align: center;
                margin-top: 200px;
                width: 450px;
            }
            input {
                margin: 20px 0;
            }
            #weather {
                margin-top: 15px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Jaka jest pogoda?</h1>
            <form>
                <div class="form-group">
                    <label for="city">Wpisz nazwę miasta</label>
                    <input type="text" class="form-control" id="city" name="city" placeholder="np: Gdańsk" value="<?php
if (array_key_exists('city', $_GET)) {
    echo $_GET['city'];
}
?>">
                </div>
                <button type="submit" class="btn btn-primary">Sprawdź</button>
            </form>
            <div id="weather">
                <?php
                if ($weather) {
                    echo '<div class="alert alert-info" role="alert"><strong>' . $weather . '</strong></div>';
                }
                else if ($error) {

                    echo '<div class="alert alert-danger" role="alert"><strong>' . $error . '</strong></div>';
                }
                ?>
            </div>
        </div>

        <!-- jQuery first, then Tether, then Bootstrap JS. -->
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    </body>
</html>