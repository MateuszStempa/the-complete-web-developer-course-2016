<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        
        <style type="text/css">

            .container {
                
                text-align: center;
                width: 450px;
            }

            #homePageConteiner {
                
                margin-top: 150px;
            }
            #containerLoggedInPage {

                margin-top: 60px;
            }

            html { 
                
                background: url(09_background.jpg) no-repeat center center fixed; 
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }
            body {
                
                background: none;
                color: white;
            }

            #logInForm {
                
                display: none;
            }

            #diary {
                
                width: 100%;
                height: 100%;
            }
            
        </style>
        
    </head>
    <body>