<p>Jak masz na imię?</p>

<form method="post">

    <input name="name" type="text">

    <input type="submit" value="Kliknij!">

</form>

<?php

//print_r($_POST);

echo "<br><br><hr>";

if ($_POST) {
    
    $users = array("Mateusz", "Rafał", "Tomek", "Bartek", "Kamil");
    
    $isKnown = FALSE;
    
    foreach ($users as $value) {
        
        if ($value == $_POST['name']) {
            
            $isKnown = TRUE;
            
        }
        
    }
    
    if ($isKnown) {
        echo "Cześć " . $_POST['name']."!";
    }
    else {

        echo "Nie znam Ciebie!";
    }
}
