<?php

$myArray = array("Mateusz", "Tomek", "Bartek", "Rafał");

$myArray[] = "Kasia";

unset($myArray[3]);

print_r($myArray);

echo "<br><br>";

echo $myArray[2];

echo "<br><br>";

$anotherArray[0] = "lody";

$anotherArray[1] = "gofry";

$anotherArray[4] = "zapiekanka";

$anotherArray["ulubione"] = "shake";

print_r($anotherArray);

echo "<br><br>";

echo $anotherArray["ulubione"];

echo "<br><br>";

$thirdArray = array(
    "Francja" => "Francuski",
    "USA" => "Angielski",
    "Polska" => "Polski",
    "Niemcy" => "Niemiecki");

unset($thirdArray["Francja"]);

print_r($thirdArray);

echo "<br><br>";

echo sizeof($myArray);
