<?php
session_start();

$error = "";

if (array_key_exists("logout", $_GET)) {

    unset($_SESSION);
    setcookie("id", "", time() - 60 * 60);
    $_COOKIE["id"] = "";
}
else if ((array_key_exists("id", $_SESSION) AND $_SESSION['id'])
        OR ( array_key_exists("id", $_COOKIE) AND $_COOKIE['id'])) {

    header("Location: 09_loggedinpage.php");
}


if (array_key_exists("submit", $_POST)) {

    include("09_connection.php");

    if (!$_POST['email']) {

        $error .= "Wymagany jest adres email.<br>";
    }

    if (!$_POST['password']) {

        $error .= "Wymagane jest hasło.<br>";
    }

    if ($error != "") {

        $error = "<p>Uzupełnij pola:</p>" . $error;
    }
    else {

        if ($_POST['singUp'] == 1) {

            $query = "SELECT id FROM `users` WHERE email = '"
                    . mysqli_real_escape_string($link, $_POST['email']) . "' LIMIT 1";

            $result = mysqli_query($link, $query);

            if (mysqli_num_rows($result) > 0) {

                $error = "Ten email jest juz zajęty";
            }
            else {

                $query = "INSERT INTO `users` (`email`,`password`) VALUES "
                        . "('" . mysqli_real_escape_string($link, $_POST['email']) . "',"
                        . "'" . mysqli_real_escape_string($link, $_POST['password']) . "')";

                if (!mysqli_query($link, $query)) {

                    $error = "<p>Wystąpił błąd - proszę spróbować ponownie</p>";
                }
                else {

                    $query = "UPDATE `users` SET password = '"
                            . md5(md5(mysqli_insert_id($link))
                                    . $_POST['password']) . "' WHERE id = "
                            . mysqli_insert_id($link) . " LIMIT 1";

                    mysqli_query($link, $query);

                    $_SESSION['id'] = mysqli_insert_id($link);

                    if ($_POST['stayLoggedIn'] == 1) {

                        setcookie("id", $row['id'], time() + 60 * 60 * 24 * 365);
                    }

                    header("Location: 09_loggedinpage.php");
                }
            }
        }
        else {

            $query = "SELECT * FROM `users` WHERE email = '" . mysqli_real_escape_string($link, $_POST['email']) . "'";

            $result = mysqli_query($link, $query);

            $row = mysqli_fetch_array($result);

            if (isset($row)) {

                $hashedPassword = md5(md5($row['id']) . $_POST['password']);

                if ($hashedPassword == $row['password']) {

                    $_SESSION['id'] = $row['id'];

                    if ($_POST['stayLoggedIn'] == 1) {

                        setcookie("id", mysqli_insert_id($link), time() + 60 * 60 * 24 * 365);
                    }

                    header("Location: 09_loggedinpage.php");
                }
                else {

                    $error = "Email lub hasło jest niepoprawne!";
                }
            }
            else {

                $error = "Email lub hasło jest niepoprawne!";
            }
        }
    }
}
?>

<?php include("09_header.php"); ?>

<div class="container" id="homePageConteiner">

    <h1>Szybki Pamiętnik</h1>

    <p><strong>Przechowój swoje przemyślenia szybko i bezpiecznie.</strong></p>

    <div id="error"><?php if ($error != "") { echo '<div class="alert alert-danger" role="alert">'.$error.'</div>'; } ?></div>

    <form method="post" id="singUpForm">
        <p>Zainteresowany? Zarejestruj się teraz!</p>
        <fieldset class="form-group">
            <input type="email" class="form-control" name="email" placeholder="Twój Email">
        </fieldset>
        <fieldset class="form-group">
            <input type="password" class="form-control" class="form-control" name="password" placeholder="Hasło">
        </fieldset>
        <div class="form-check">
            <label class="form-check-label">
                <input type="checkbox" class="form-check-input" name="stayLoggedIn" value="1">Pozostań zalogowany.
            </label>
        </div>
        <fieldset class="form-group">
            <input type="hidden" name="singUp" value="1">
            <input type="submit" class="btn btn-success" name="submit" value="Zarejestruj!">
        </fieldset>
        <p><a class="toggleUpForms btn btn-primary">Logowanie</a></p>
    </form>

    <form method="post" id="logInForm">
        <p>Zalogu się za pomocą twojego adresu email oraz hasła.</p>
        <fieldset class="form-group">
            <input type="email" class="form-control" name="email" placeholder="Twój Email">
        </fieldset>
        <fieldset class="form-group">
            <input type="password" class="form-control" name="password" placeholder="Hasło">
        </fieldset>
        <div class="form-check">
            <label class="form-check-label">
                <input type="checkbox" class="form-check-input" name="stayLoggedIn" value="1">Pozostań zalogowany.
            </label>
        </div>
        <fieldset class="form-group">
            <input type="hidden" name="singUp" value="0">
            <input type="submit" class="btn btn-success" name="submit" value="Zaloguj!">
        </fieldset>
        <p><a class="toggleUpForms btn btn-primary">Rejestracja</a></p>
    </form>
</div>

<?php include("09_footer.php"); ?>

